const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {BuyModel,SellModel,ShortSellModel,CloseShortModel} = require('./models');

class CalculateClass {
  constructor() {

  }

  async buyLeftInPortfolio() {
    //get buy data
    let buyRes = await BuyModel.findAll({
    })

    //get sell data
    var leftArr = []
    for (let i=0;i<buyRes.length;i++) {
      let buy_id = buyRes[i].id
      let sellRes = await SellModel.findAll({
        where: {
          buy_id:buy_id
        }
      })
      //add no sell
      if (sellRes.length == 0) {
        leftArr.push(buyRes[i])
      }
    }

    return leftArr
  }

  async shortLeftInPortfolio() {
    //get buy data
    let shortRes = await ShortSellModel.findAll({
    })

    //get sell data
    var leftArr = []
    for (let i=0;i<shortRes.length;i++) {
      let short_id = shortRes[i].id
      let closeShortRes = await CloseShortModel.findAll({
        where: {
          short_sell_id:short_id
        }
      })
      //add no sell
      if (closeShortRes.length == 0) {
        leftArr.push(shortRes[i])
      }
    }

    return leftArr
  }

  async profitList() {
    //get buy data
    let buyRes = await BuyModel.findAll({
    })

    //get sell data
    var profitRes = []
    for (let i=0;i<buyRes.length;i++) {
      let buy_id = buyRes[i].id
      let sellRes = await SellModel.findAll({
        where: {
          buy_id:buy_id
        }
      })

      //check if there is selling
      if (sellRes.length != 0) {
        let profit = {
          symbol : buyRes[i].symbol,
          profit : sellRes[0].price - buyRes[i].price,
          buy_date : buyRes[i].updatedAt,
          sell_date : sellRes[0].updatedAt
        }
        profitRes.push(profit)
      }
    }

    return profitRes
  }

  async getHistory() {
    //close short
    CloseShortModel.belongsTo(ShortSellModel,{foreignKey:'short_sell_id'})
    let csres = await CloseShortModel.findAll({
      include:[ShortSellModel],
      order: [
          ['updatedAt','DESC']
      ]
    })

    let csFilteredRes = []
    csres.forEach((e,idx) => {
      let data = {
        type:'short',
        close_short_id:e.short_sell_id,
        close_short_price:e.price,
        close_short_updatedAt:e.updatedAt,
        short_id:e.short_sell.id,
        short_symbol:e.short_sell.symbol,
        short_amount:e.short_sell.amount,
        short_price:e.short_sell.price,
        short_stop_loss:e.short_sell.stop_loss,
        short_target_price:e.short_sell.target_price,
        short_updatedAt:e.short_sell.updatedAt,
        updatedAt:e.updatedAt
      }

      csFilteredRes.push(data)
    })

    //selling
    SellModel.belongsTo(BuyModel,{foreignKey:'buy_id'})
    let mres = await SellModel.findAll({
      include:[BuyModel],
      order: [
          ['updatedAt','DESC']
      ]

    })

    //console.log('--------------------------')
    //console.log(JSON.stringify(mres))
    let sres = []
    mres.forEach((e,idx) => {
      let data = {
        type:'buy',
        sell_id:e.id,
        sell_price:e.price,
        sell_updatedAt:e.updatedAt,
        buy_id:e.buy.id,
        symbol:e.buy.symbol,
        amount:e.buy.amount,
        buy_price:e.buy.price,
        stop_loss:e.buy.stop_loss,
        target_price:e.buy.target_price,
        buy_updatedAt : e.buy.updatedAt,
        updatedAt:e.updatedAt
      }
      sres.push(data)
    })

    //merge and sort
    let concatRes = csFilteredRes.concat(sres)
    concatRes.sort((a,b) => {
      return this.toTimestamp(b.updatedAt) - this.toTimestamp(a.updatedAt)
    })

    //console.log(this.toTimestamp(concatRes[0].updatedAt))
    //console.log(concatRes)

    return concatRes
  }

  async getPortfolio() {
    //get buy
    let bres = await BuyModel.findAll({
      order: [
          ['updatedAt','DESC']
      ]
    })
      //modify data
    let bres2 = []
    bres.forEach((e,idx) => {
      let json = {
        buy_id:e.id,
        symbol:e.symbol,
        amount:e.amount,
        price:e.price,
        stop_loss:e.stop_loss,
        target_price:e.target_price,
        updatedAt:e.updatedAt
      }
      bres2.push(json)
    })
    //get sell
    let sres = await SellModel.findAll({
      order: [
          ['updatedAt','DESC']
      ]
    })
    let sresIdArr = []
    sres.forEach((e,idx) => {
      sresIdArr.push(e.buy_id)
    })
    //filter unwant buy data
    let buyFinalRes = bres2.filter((buy) => {
      if (sresIdArr.indexOf(buy.buy_id) == -1)
        return buy
    })
    //short
    let ssres = await ShortSellModel.findAll({
      order: [
          ['updatedAt','DESC']
      ],
      raw:true
    })
    //close short
    let csres = await CloseShortModel.findAll({
      order: [
          ['updatedAt','DESC']
      ],
      raw:true
    })
    let csIdArr = csres.map(cs => cs.short_sell_id)

    let shortFinalRes = ssres.filter((ss) => {
      if (csIdArr.indexOf(ss.id) == -1)
        return ss
    })

    //concat
      //add type buy
    let buyFinalRes2 = buyFinalRes.map((b) => {
      b.type = 'buy'
      return b
    })
      //add type short
    let shortFinalRes2 = shortFinalRes.map((s) => {
      s.type = 'short'
      return s
    })
    //concat
    let concatRes = buyFinalRes2.concat(shortFinalRes2)
    //sort
    concatRes.sort((a,b) => {
      return this.toTimestamp(b.updatedAt) - this.toTimestamp(a.updatedAt)
    })

    return concatRes
  }

  toTimestamp(strDate) {
     var datum = Date.parse(strDate)
     return datum/1000
   }

}

module.exports = CalculateClass
