const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {CloseShortModel,ShortSellModel} = require('./models');

class CloseShortClass {
  constructor() {

  }

  create(res,short_sell_id,price) {
    CloseShortModel.create({
      short_sell_id:short_sell_id,
      price:price,

    }).then(()=>{
      res.send('ok')
    })
  }

  update(res,id,short_sell_id,price) {
    let data = {
      short_sell_id:short_sell_id,
      price:price,
    }
    CloseShortModel.update(data, {
          where: {
            id:id
          }
    }).then(() => {
      res.send('ok')
    })
  }

  delete(res,id) {
    CloseShortModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      res.send('ok')
    })

  }

  async get(res,id) {
    let mres

    CloseShortModel.belongsTo(ShortSellModel,{foreignKey:'short_sell_id'})

    if (id != -1) {
       mres = await CloseShortModel.findAll({
        where: {
          id:id
        },
        include:[ShortSellModel]
      })
    } else {
      mres = await CloseShortModel.findAll({
        include:[ShortSellModel]
      })
    }

    res.json(mres)
  }

}

module.exports = CloseShortClass
