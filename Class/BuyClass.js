const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {BuyModel,SellModel} = require('./models');

class BuyClass {
  constructor() {

  }

  create(res,symbol,amount,price,stop_loss,target_price) {
    let m = BuyModel.create({
      symbol:symbol,
      amount:amount,
      price:price,
      stop_loss:stop_loss,
      target_price:target_price
    })

    res.send('ok')

  }

  update(res,id,symbol,amount,price,stop_loss,target_price) {
    let data = {
      symbol:symbol,
      amount:amount,
      price:price,
      stop_loss:stop_loss,
      target_price : target_price
    }
    BuyModel.update(data, {
          where: {
            id:id
          }
    }).then(() => {
      res.send('ok')
    })
  }

  delete(res,id) {
    //delete sell
    SellModel.destroy({
      where:{
        buy_id:id
      }
    }).then(() => {
      res.send('delete sell ok')
    })

    //delete buy
    BuyModel.destroy({
      where: {
        id: id
      }
    }).then(() => {

      res.send('delete buy ok')
    })

  }

  async get(res,id) {
    let mres

    if (id != -1) {
       mres = await BuyModel.findAll({
        where: {
          id:id
        },
        order: [['updatedAt','DESC']]
      })
    } else {
      mres = await BuyModel.findAll()
    }

    res.json(mres)
  }

}

module.exports = BuyClass
