const Sequelize = require('sequelize');
const Config = require('../config.js')

const seqConn = new Sequelize(Config.databaseName, Config.databaseUserName, Config.databasePassword, {
  host: 'localhost',
  dialect: 'mariadb',
  dialectOptions: {
    // useUTC: false, //for reading from database
    dateStrings: true,
    typeCast: true,
    timezone: "+07:00"
  },
  timezone: "+07:00", //for writing to database
  
});

module.exports = {seqConn}
