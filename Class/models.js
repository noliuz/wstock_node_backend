const Sequelize = require('sequelize');
const {seqConn} = require('./seqConn')

const BuyModel = seqConn.define('buy', {
  symbol : {
    type: Sequelize.STRING
  },
  amount : {
    type: Sequelize.INTEGER
  },
  price : {
    type: Sequelize.FLOAT
  },
  stop_loss : {
    type: Sequelize.FLOAT
  },
  target_price : {
    type: Sequelize.FLOAT
  },
}, {
  tableName: 'buy'
})

const SellModel = seqConn.define('sell', {
  buy_id : {
    type: Sequelize.INTEGER
  },
  price : {
    type: Sequelize.FLOAT
  },

}, {
  tableName: 'sell'
})

const ShortSellModel = seqConn.define('short_sell', {
  symbol : {
    type: Sequelize.STRING
  },
  amount : {
    type: Sequelize.INTEGER
  },
  price : {
    type: Sequelize.FLOAT
  },
  stop_loss : {
    type: Sequelize.FLOAT
  },
  target_price : {
    type: Sequelize.FLOAT
  },

}, {
  tableName: 'short_sell'
})

const LongBuyModel = seqConn.define('long_buy', {
  symbol : {
    type: Sequelize.STRING
  },
  amount : {
    type: Sequelize.INTEGER
  },
  price : {
    type: Sequelize.FLOAT
  },
  stop_loss : {
    type: Sequelize.FLOAT
  },
  target_price : {
    type: Sequelize.FLOAT
  },

}, {
  tableName: 'long_buy'
})

const CloseShortModel = seqConn.define('close_short', {
  short_sell_id : {
    type: Sequelize.INTEGER
  },
  price : {
    type: Sequelize.FLOAT
  },

}, {
  tableName: 'close_short'
})

const CloseLongModel = seqConn.define('close_long', {
  long_buy_id : {
    type: Sequelize.INTEGER
  },
  price : {
    type: Sequelize.FLOAT
  },

}, {
  tableName: 'close_long'
})

module.exports = {BuyModel,SellModel,LongBuyModel,ShortSellModel,
          CloseLongModel,CloseShortModel}
