const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {LongBuyModel} = require('./models');

class LongBuyClass {
  constructor() {

  }

  create(res,symbol,amount,price,stop_loss,target_price) {
    LongBuyModel.create({
      symbol:symbol,
      amount:amount,
      price:price,
      stop_loss:stop_loss,
      target_price:target_price
    }).then(()=>{
      res.send('ok')
    })
  }

  update(res,id,symbol,amount,price,stop_loss,target_price) {
    let data = {
      symbol:symbol,
      amount:amount,
      price:price,
      stop_loss:stop_loss,
      target_price:target_price
    }
    LongBuyModel.update(data, {
          where: {
            id:id
          }
    }).then(() => {
      res.send('ok')
    })
  }

  delete(res,id) {
    LongBuyModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      res.send('ok')
    })

  }

  async get(res,id) {
    let mres

    if (id != -1) {
       mres = await LongBuyModel.findAll({
        where: {
          id:id
        }
      })
    } else {
      mres = await LongBuyModel.findAll();
    }

    res.json(mres)
  }

}

module.exports = LongBuyClass
