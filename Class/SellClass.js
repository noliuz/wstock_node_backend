const Sequelize = require('sequelize')
const {seqConn} = require('./seqConn')
const {SellModel,BuyModel} = require('./models');

class SellClass {
  constructor() {

  }

  create(res,buy_id,price) {
    let m = SellModel.create({
      buy_id:buy_id,
      price:price
    }).then(()=>{
      res.send('ok')
    })

  }

  update(res,id,buy_id,price) {
    let data = {
      buy_id:buy_id,
      price:price
    }
    SellModel.update(data, {
          where: {
            id:id
          }
    }).then(() => {
      res.send('ok')
    })
  }

  delete(res,id) {
    SellModel.destroy({
      where: {
        id: id
      }
    }).then(() => {
      res.send('ok')
    })

  }

  async get(res,id) {
    let mres
    SellModel.belongsTo(BuyModel,{foreignKey:'buy_id'})

    if (id != -1) {
       mres = await SellModel.findAll({
        where: {
          id:id
        },
        include:[BuyModel]
      })
    } else {
      mres = await SellModel.findAll({
        include:[BuyModel]
      })
    }

    res.json(mres)
  }

}

module.exports = SellClass
